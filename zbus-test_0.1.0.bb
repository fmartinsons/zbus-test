inherit cargo cargo-update-recipe-crates

SRC_URI += "git://git@gitlab.com/fmartinsons/zbus-test.git;protocol=ssh;branch=main"
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

require ${BPN}-crates.inc

SUMMARY = "zbus-test"
HOMEPAGE = "https://gitlab.com/fmartinsons/zbus-test.git"
LICENSE = "CLOSED"

# includes this file if it exists but does not fail
# this is useful for anything you may want to override from
# what cargo-bitbake generates.
include zbus-test-${PV}.inc
include zbus-test.inc
