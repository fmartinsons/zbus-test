use zbus::{Connection, Result};
use zbus::fdo::DBusProxy;

#[tokio::main(flavor = "current_thread")]
async fn dbus() -> Result<()> {
    let connection = Connection::system().await?;
    let proxy = DBusProxy::new(&connection).await?;

    Ok(())
}

fn main() {
    println!("Hello world zbus");
    match dbus() {
        Result::Ok(()) => println!("Call OK"),
        Result::Err(e) => panic!("Ouch: {e}")
    }
}
