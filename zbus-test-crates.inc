# Autogenerated with 'bitbake -c update_crates zbus-test'

# from Cargo.lock
SRC_URI += " \
    crate://crates.io/aho-corasick/0.7.20 \
    crate://crates.io/async-broadcast/0.5.1 \
    crate://crates.io/async-channel/1.8.0 \
    crate://crates.io/async-executor/1.5.0 \
    crate://crates.io/async-fs/1.6.0 \
    crate://crates.io/async-io/1.12.0 \
    crate://crates.io/async-lock/2.7.0 \
    crate://crates.io/async-recursion/1.0.2 \
    crate://crates.io/async-task/4.3.0 \
    crate://crates.io/async-trait/0.1.66 \
    crate://crates.io/atomic-waker/1.1.0 \
    crate://crates.io/autocfg/1.1.0 \
    crate://crates.io/bitflags/1.3.2 \
    crate://crates.io/block-buffer/0.10.4 \
    crate://crates.io/blocking/1.3.0 \
    crate://crates.io/byteorder/1.4.3 \
    crate://crates.io/bytes/1.4.0 \
    crate://crates.io/cc/1.0.79 \
    crate://crates.io/cfg-if/1.0.0 \
    crate://crates.io/concurrent-queue/2.1.0 \
    crate://crates.io/cpufeatures/0.2.5 \
    crate://crates.io/crossbeam-utils/0.8.15 \
    crate://crates.io/crypto-common/0.1.6 \
    crate://crates.io/derivative/2.2.0 \
    crate://crates.io/digest/0.10.6 \
    crate://crates.io/dirs-sys/0.3.7 \
    crate://crates.io/dirs/4.0.0 \
    crate://crates.io/enumflags2/0.7.5 \
    crate://crates.io/enumflags2_derive/0.7.4 \
    crate://crates.io/errno-dragonfly/0.1.2 \
    crate://crates.io/errno/0.2.8 \
    crate://crates.io/event-listener/2.5.3 \
    crate://crates.io/fastrand/1.9.0 \
    crate://crates.io/futures-core/0.3.27 \
    crate://crates.io/futures-io/0.3.27 \
    crate://crates.io/futures-lite/1.12.0 \
    crate://crates.io/futures-sink/0.3.27 \
    crate://crates.io/futures-task/0.3.27 \
    crate://crates.io/futures-util/0.3.27 \
    crate://crates.io/generic-array/0.14.6 \
    crate://crates.io/getrandom/0.2.8 \
    crate://crates.io/hashbrown/0.12.3 \
    crate://crates.io/hex/0.4.3 \
    crate://crates.io/indexmap/1.9.2 \
    crate://crates.io/instant/0.1.12 \
    crate://crates.io/io-lifetimes/1.0.6 \
    crate://crates.io/lazy_static/1.4.0 \
    crate://crates.io/libc/0.2.140 \
    crate://crates.io/linux-raw-sys/0.1.4 \
    crate://crates.io/log/0.4.17 \
    crate://crates.io/memchr/2.5.0 \
    crate://crates.io/memoffset/0.7.1 \
    crate://crates.io/mio/0.8.6 \
    crate://crates.io/nix/0.26.2 \
    crate://crates.io/once_cell/1.17.1 \
    crate://crates.io/ordered-stream/0.2.0 \
    crate://crates.io/parking/2.0.0 \
    crate://crates.io/pin-project-lite/0.2.9 \
    crate://crates.io/pin-utils/0.1.0 \
    crate://crates.io/polling/2.6.0 \
    crate://crates.io/ppv-lite86/0.2.17 \
    crate://crates.io/proc-macro-crate/1.3.1 \
    crate://crates.io/proc-macro2/1.0.52 \
    crate://crates.io/quote/1.0.26 \
    crate://crates.io/rand/0.8.5 \
    crate://crates.io/rand_chacha/0.3.1 \
    crate://crates.io/rand_core/0.6.4 \
    crate://crates.io/redox_syscall/0.2.16 \
    crate://crates.io/redox_users/0.4.3 \
    crate://crates.io/regex-syntax/0.6.28 \
    crate://crates.io/regex/1.7.1 \
    crate://crates.io/rustix/0.36.9 \
    crate://crates.io/serde/1.0.155 \
    crate://crates.io/serde_derive/1.0.155 \
    crate://crates.io/serde_repr/0.1.11 \
    crate://crates.io/sha1/0.10.5 \
    crate://crates.io/slab/0.4.8 \
    crate://crates.io/socket2/0.4.9 \
    crate://crates.io/static_assertions/1.1.0 \
    crate://crates.io/syn/1.0.109 \
    crate://crates.io/tempfile/3.4.0 \
    crate://crates.io/thiserror-impl/1.0.39 \
    crate://crates.io/thiserror/1.0.39 \
    crate://crates.io/tokio-macros/1.8.2 \
    crate://crates.io/tokio/1.26.0 \
    crate://crates.io/toml_datetime/0.6.1 \
    crate://crates.io/toml_edit/0.19.6 \
    crate://crates.io/tracing-attributes/0.1.23 \
    crate://crates.io/tracing-core/0.1.30 \
    crate://crates.io/tracing/0.1.37 \
    crate://crates.io/typenum/1.16.0 \
    crate://crates.io/uds_windows/1.0.2 \
    crate://crates.io/unicode-ident/1.0.8 \
    crate://crates.io/version_check/0.9.4 \
    crate://crates.io/waker-fn/1.1.0 \
    crate://crates.io/wasi/0.11.0+wasi-snapshot-preview1 \
    crate://crates.io/winapi-i686-pc-windows-gnu/0.4.0 \
    crate://crates.io/winapi-x86_64-pc-windows-gnu/0.4.0 \
    crate://crates.io/winapi/0.3.9 \
    crate://crates.io/windows-sys/0.42.0 \
    crate://crates.io/windows-sys/0.45.0 \
    crate://crates.io/windows-targets/0.42.2 \
    crate://crates.io/windows_aarch64_gnullvm/0.42.2 \
    crate://crates.io/windows_aarch64_msvc/0.42.2 \
    crate://crates.io/windows_i686_gnu/0.42.2 \
    crate://crates.io/windows_i686_msvc/0.42.2 \
    crate://crates.io/windows_x86_64_gnu/0.42.2 \
    crate://crates.io/windows_x86_64_gnullvm/0.42.2 \
    crate://crates.io/windows_x86_64_msvc/0.42.2 \
    crate://crates.io/winnow/0.3.5 \
    crate://crates.io/zbus/3.11.0 \
    crate://crates.io/zbus_macros/3.11.0 \
    crate://crates.io/zbus_names/2.5.0 \
    crate://crates.io/zvariant/3.12.0 \
    crate://crates.io/zvariant_derive/3.12.0 \
    crate://crates.io/zvariant_utils/1.0.0 \
"
